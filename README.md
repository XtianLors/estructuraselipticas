# EstructurasElipticas

Proyecto donde analizamos estructuras elítpticas.

Este tipo de estructuras se utilizan mucho en ingeniería como elementos estructurales fundamentales.
El propósito de este repositorio es presentar el aspecto matemático de las estrucuturas elípticas también llamadas, estructuras con planas. El enfoque de este análisis se centra en:


- Revisión de las teorías más populares de las esctructuras planas o elípticas.
- La relación entre las teorías 2D y 3D.
- La presentación de teorías recientemente desarrolladas de estructuras planas como la teoría micropolar y el gradiante tipado.
- La aplicación de las estructuras elípticas en el modelado de sistemas complejos (planos hechos de espuma, planos autoensamblados, dobleces múltiples)
- Modelado de nano películas y nanotubos.
